<?php

namespace App\Http\Controllers;

use App\Enums\TransactionTypeEnum;
use App\Http\Requests\TransactionCreateRequest;
use App\Models\Transaction;
use App\Services\StorageService;
use App\Services\TopupService;
use App\Services\TransactionService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    protected $storageService;

    public function __construct()
    {
        $this->storageService = new StorageService;
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        $account = $user->account;
        $balance = $account->balance;
        $typeOptions = TransactionTypeEnum::list();
        $query = Transaction::where('account_id', $account->id)->latest();

        if ($request->search) {
            $query->where('code', 'LIKE', '%' . $request->search . '%')
                ->orWhere('notes', 'LIKE', '%' . $request->search . '%');
        }

        if ($request->type) {
            $query->where('type', $request->type);
        }

        if ($request->start_date) {
            $query->whereDate('created_at', '>=', $request->start_date);
        }

        if ($request->end_date) {
            $query->whereDate('created_at', '<=', $request->end_date);
        }

        $transactions = $query->paginate(5);

        return view('transaction.index', compact('balance', 'transactions', 'typeOptions'));
    }

    public function create()
    {
        $typeOptions = TransactionTypeEnum::list();

        return view('transaction.create', compact('typeOptions'));
    }

    public function store(TransactionCreateRequest $request)
    {
        $user = auth()->user();
        $account = $user->account;
        $attributes = $request->only(['type', 'amount', 'notes']);
        $attributes['account_id'] = $account->id;

        if (
            $request->type === TransactionTypeEnum::TRANSACTION->value &&
            $account->balance < $request->amount
        ) {
            return back()->withErrors('Insufficient balance');
        }

        if ($request->type === TransactionTypeEnum::TOPUP->value) {
            (new TopupService($request, $account))->process();
        } elseif ($request->type === TransactionTypeEnum::TRANSACTION->value) {
            (new TransactionService($request, $account))->process();
        }

        return redirect()->route('home');
    }
}
