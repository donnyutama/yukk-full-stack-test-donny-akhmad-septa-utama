<?php

namespace App\Http\Requests;

use App\Enums\TransactionTypeEnum;
use Illuminate\Foundation\Http\FormRequest;

class TransactionHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $typeList = TransactionTypeEnum::list();

        return [
            'search' => ['string'],
            'type' => ['string', 'in:' . (implode(',', $typeList))],
            'start_date' => ['date'],
            'end_date' => ['date'],
        ];
    }
}
