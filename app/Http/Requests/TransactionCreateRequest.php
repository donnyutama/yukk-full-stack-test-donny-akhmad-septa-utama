<?php

namespace App\Http\Requests;

use App\Enums\TransactionTypeEnum;
use Illuminate\Foundation\Http\FormRequest;

class TransactionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $typeList = TransactionTypeEnum::list();

        return [
            'type' => ['required', 'string', 'in:' . (implode(',', $typeList))],
            'amount' => ['required', 'numeric', 'gt:0'],
            'notes' => ['string'],
            'proof_file' => ['required_if:type,' . TransactionTypeEnum::TOPUP->value, 'nullable', 'image', 'max:2048'],
        ];
    }
}
