<?php

namespace App\Enums;

use BackedEnum;
use Closure;

enum TransactionTypeEnum: string
{
    case TOPUP = 'topup';
    case TRANSACTION = 'transaction';

    public static function list()
    {
        $cases = static::cases();

        return array_column($cases, 'value');
    }
}
