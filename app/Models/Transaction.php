<?php

namespace App\Models;

use App\Services\StorageService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    protected $storageService;

    public function __construct()
    {
        parent::__construct();

        $this->storageService = new StorageService;
    }

    protected $fillable = [
        'type',
        'account_id',
        'notes',
        'amount',
        'proof_file',
        'closing_balance',
    ];

    protected $append = [
        'proof_file_url',
    ];

    protected static function booted()
    {
        parent::booted();

        static::creating(function ($model) {
            $timestamps = Carbon::now()->getTimestampMs();

            if (empty($model->code)) {
                $model->code = sprintf('TRX-%s', $timestamps);
            }
        });
    }

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }

    public function getProofFileUrlAttribute(): string
    {
        return $this->storageService->url($this->proof_file);
    }
}
