<?php

namespace App\Services;

use App\Http\Requests\TransactionCreateRequest;
use App\Models\Account;
use App\Models\Transaction;

class TransactionService
{
    protected $account;
    protected $data;
    protected $request;

    public function __construct(TransactionCreateRequest $request, Account $account)
    {
        $this->account = $account;
        $this->data = $request->only(['type', 'amount', 'notes']);
        $this->data['account_id'] = $account->id;
        $this->request = $request;
    }

    public function process()
    {
        $attributes = $this->data;

        $transaction = Transaction::create($attributes);

        $this->account->balance -= $attributes['amount'];
        $this->account->save();

        $transaction->update(['closing_balance' => $this->account->balance]);
    }
}
