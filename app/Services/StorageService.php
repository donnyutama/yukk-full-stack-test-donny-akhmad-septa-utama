<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class StorageService
{
    protected $storage;

    public function __construct($disk = 'local')
    {
        $this->storage = Storage::disk($disk);
    }

    public function store($file, $path = 'uploads', $filename = null)
    {
        if (!isset($filename)) {
            $filename = $file->hashName();
        } else {
            $extension = $file->extension();
            $filename = $filename . '.' . $extension;
        }

        return Storage::putFileAs($path, $file, $filename);
    }

    public function url($filePath)
    {
        return Storage::url($filePath);
    }
}
