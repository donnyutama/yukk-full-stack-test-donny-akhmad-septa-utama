<?php

namespace App\Services;

use App\Http\Requests\TransactionCreateRequest;
use App\Models\Account;
use App\Models\Transaction;
use App\Services\StorageService;

class TopupService
{
    protected $account;
    protected $data;
    protected $request;
    protected $storageService;

    public function __construct(TransactionCreateRequest $request, Account $account)
    {
        $this->account = $account;
        $this->data = $request->only(['type', 'amount', 'notes']);
        $this->data['account_id'] = $account->id;
        $this->request = $request;
        $this->storageService = new StorageService;
    }

    public function process()
    {
        $attributes = $this->data;

        $directory = sprintf('public/proof/%s', date('Ymd'));
        $filePath = $this->storageService->store($this->request->proof_file, $directory);
        $attributes['proof_file'] = $filePath;

        $transaction = Transaction::create($attributes);

        $this->account->balance += $attributes['amount'];
        $this->account->save();

        $transaction->update(['closing_balance' => $this->account->balance]);
    }
}
