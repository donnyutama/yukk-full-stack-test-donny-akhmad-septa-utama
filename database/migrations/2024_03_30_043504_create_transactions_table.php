<?php

use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    protected $account;
    protected $transaction;

    public function __construct()
    {
        $this->account = (new Account)->getTable();
        $this->transaction = (new Transaction)->getTable();
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->transaction, function (Blueprint $table) {
            $table->id();
            $table->string('code', 20);
            $table->string('type', 15);
            $table->unsignedBigInteger('account_id');
            $table->string('notes', 50)->nullable();
            $table->decimal('amount');
            $table->string('proof_file')->nullable();
            $table->decimal('closing_balance');
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on($this->account);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->transaction);
    }
};
