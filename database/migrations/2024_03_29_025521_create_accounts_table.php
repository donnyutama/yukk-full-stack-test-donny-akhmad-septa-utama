<?php

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    protected $account;
    protected $user;

    public function __construct()
    {
        $this->account = (new Account)->getTable();
        $this->user = (new User)->getTable();
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->account, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->decimal('balance')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on($this->user);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->account);
    }
};
