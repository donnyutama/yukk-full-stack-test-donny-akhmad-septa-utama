@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h1>IDR {{ number_format(Auth::user()->account->balance, 2, ',', '.') }}</h1>
                            <div>Account Number: 1{{ str_pad(Auth::user()->account->id, 9, '0', STR_PAD_LEFT) }}</div>
                        </div>
                        <div class="col-6 text-end">
                            <a href="{{ route('transactions.create') }}" class="btn btn-success mb-3">Tambah Transaksi</a><br>
                            <a href="{{ route('transactions.index') }}" class="btn btn-info">Lihat Riwayat Transaksi</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
