@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="mb-3">
                <a href="{{ route('home') }}">Back</a>
            </div>
            <form action="{{ route('transactions.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">Buat Transaksi</div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="row mb-3">
                                    <div class="col-sm-12">
                                        @include('error-summary')
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="" class="col-sm-4 col-form-label">Tipe Transaksi</label>
                                    <div class="col-sm-8">
                                        <select name="type" class="form-select" id="tipe-transaksi" required>
                                            <option value="">Pilih</option>
                                            @foreach ($typeOptions as $option)
                                                <option value="{{ $option }}" @selected(old('type') == $option)>
                                                    {{ ucwords($option) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="" class="col-sm-4 col-form-label">Nominal</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-text">IDR</div>
                                            <input type="text" class="form-control text-end" id="nominal"
                                                placeholder="0" name="amount" value="{{ old('nominal') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="" class="col-sm-4 col-form-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="notes" rows="2"></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3 d-none" id="topup-bukti">
                                    <label for="" class="col-sm-4 col-form-label">Upload Bukti</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="file" name="proof_file" id="bukti-file"
                                            accept="image/*">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="offset-sm-4 col-sm-8">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script type="module">
        var pageJs = {
            numberWithCommas: function(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            init: function() {
                $("#tipe-transaksi").trigger("change");

                $("#tipe-transaksi").change(function() {
                    let value = $(this).val();
                    console.log(value);

                    if (value === "topup") {
                        $("#topup-bukti").removeClass("d-none");
                        $("#bukti-file").attr("required", "");
                    } else {
                        $("#topup-bukti").addClass("d-none");
                        $("#bukti-file").removeAttr("required");
                    }
                });

                $("#nominal").on("keyup", function() {

                });
            }
        }

        pageJs.init();
    </script>
@endpush
