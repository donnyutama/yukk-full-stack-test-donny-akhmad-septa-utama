@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="mb-3">
                <a href="{{ route('home') }}">Back</a>
            </div>
            <div class="card">
                <div class="card-header">Buat Transaksi</div>
                <div class="card-body">
                    <div class="mb-3">
                        Current Balance: {{ number_format($balance, 2, ',', '.') }}
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <form action="" method="GET">
                                <div class="row">
                                    <div class="col-auto mb-3">
                                        <label for="search" class="form-label">Search</label>
                                        <input type="text" name="search" class="form-control"
                                            value="{{ request('search') }}" placeholder="Transaction Code or Note">
                                    </div>
                                    <div class="col-auto mb-3">
                                        <label for="search" class="form-label">Transaction Type</label>
                                        <select name="type" class="form-select">
                                            <option value="">Semua Type</option>
                                            @foreach ($typeOptions as $option)
                                                <option value="{{ $option }}" @selected(request('type') == $option)>
                                                    {{ ucwords($option) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-auto mb-3">
                                        <label for="start_date" class="form-label">Start Date</label>
                                        <input type="date" name="start_date" class="form-control"
                                            value="{{ request('start_date') }}">
                                    </div>
                                    <div class="col-auto mb-3">
                                        <label for="end_date" class="form-label">End Date</label>
                                        <input type="date" name="end_date" class="form-control"
                                            value="{{ request('end_date') }}">
                                    </div>
                                    <div class="col-auto mb-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table class="table" aria-describedby="table-history">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Transaction Code</th>
                                <th>Transaction Type</th>
                                <th>Amount</th>
                                <th>Saldo Akhir</th>
                                <th>Note</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transactions as $transaction)
                                <tr>
                                    <td>{{ $transactions->firstItem() + $loop->index }}</td>
                                    <td>{{ $transaction->created_at }}</td>
                                    <td>{{ $transaction->code }}</td>
                                    <td>{{ $transaction->type }}</td>
                                    <td>{{ number_format($transaction->amount, 2, ',', '.') }}</td>
                                    <td>{{ number_format($transaction->closing_balance, 2, ',', '.') }}</td>
                                    <td>
                                        {{ $transaction->notes }}
                                        @if ($transaction->type === 'topup')
                                            <div>
                                                <a href="{{ $transaction->proof_file_url }}" target="_blank">Bukti</a>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
