@if ($errors->any())
    <div class="alert alert-danger">
        Please fix following error:
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif